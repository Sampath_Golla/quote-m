package com.tekmob.quote_m.suppliers

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.tekmob.quote_m.R
import com.tekmob.quote_m.databinding.SupplierActivityHomeScreenBinding

class Supplier_HomeScreen : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: SupplierActivityHomeScreenBinding
    lateinit var navBottomView: BottomNavigationView
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = SupplierActivityHomeScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.supplierAppBarHomeScreen.supplierToolbar)


        drawerLayout = binding.sDrawerLayout
        navView = binding.supplierNavView
        navBottomView = findViewById(R.id.bottom_navigation_view_supplier)

        navController = findNavController(R.id.nav_host_fragment_supplier_home_screen)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_adddrivers, R.id.nav_addtruck, R.id.nav_quotedetails,
                R.id.nav_myinvoice, R.id.nav_trackorder
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navBottomView.setupWithNavController(navController)
        navView.setNavigationItemSelectedListener(this)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home_screen, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        navController = findNavController(R.id.nav_host_fragment_supplier_home_screen)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        item.isChecked = true
        drawerLayout.closeDrawers()

        val id = item.itemId
        when (id) {
//            R.id.nav_myorders ->
//
//                navController.navigate(R.id.nav_confirmation)
            R.id.nav_home ->

                navController.navigate(R.id.nav_home)
            R.id.nav_adddrivers ->

                navController.navigate(R.id.nav_adddrivers)


            R.id.nav_addtruck ->

                navController.navigate(R.id.nav_addtruck)

        }

        return true

    }

}