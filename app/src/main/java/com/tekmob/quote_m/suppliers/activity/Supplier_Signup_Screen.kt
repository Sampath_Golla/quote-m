package com.tekmob.quote_m.suppliers.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.tekmob.quote_m.R
import com.tekmob.quote_m.customer.activity.Signin_Screen

/**
 *  Created by Sucharitha Peddinti on 28/11/21.
 */
class Supplier_Signup_Screen : Activity() {
    lateinit var signup_btn: Button
    lateinit var signinBtn: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.supplier_registration_activity)
        signup_btn = findViewById(R.id.signup_btn)
        signinBtn = findViewById(R.id.signin_btn)

        signinBtn.setOnClickListener {


            val intent =
                Intent(this, Supplier_Signin_Screen::class.java)

            startActivity(intent)

            finish()
        }
        signup_btn.setOnClickListener {


            val intent = Intent(this, Supplier_Otp_Screen::class.java)

            startActivity(intent)

            finish()
        }

    }
}