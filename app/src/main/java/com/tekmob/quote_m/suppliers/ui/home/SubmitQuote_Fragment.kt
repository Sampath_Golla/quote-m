package com.tekmob.quote_m.suppliers.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.tekmob.quote_m.databinding.SubmitQuotationScreenBinding
import com.tekmob.quote_m.customer.ui.home.HomeViewModel

class SubmitQuote_Fragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: SubmitQuotationScreenBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = SubmitQuotationScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root


        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}