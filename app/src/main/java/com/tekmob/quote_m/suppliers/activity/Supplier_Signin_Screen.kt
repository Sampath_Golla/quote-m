package com.tekmob.quote_m.suppliers.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.tekmob.quote_m.HomeScreen
import com.tekmob.quote_m.customer.activity.Otp_Screen
import com.tekmob.quote_m.customer.activity.Signup_Screen
import com.tekmob.quote_m.databinding.CustomerLoginActivityBinding
import com.tekmob.quote_m.databinding.CustomerRegistrationActivityBinding
import com.tekmob.quote_m.databinding.DashboardActivityBinding
import com.tekmob.quote_m.databinding.SupplierLoginActivityBinding
import com.tekmob.quote_m.suppliers.Supplier_HomeScreen

/**
 *  Created by Sucharitha Peddinti on 28/11/21.
 */
class Supplier_Signin_Screen : Activity() {
    private lateinit var binding: SupplierLoginActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SupplierLoginActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.signinBtn.setOnClickListener {


            val intent =
                Intent(this, Supplier_HomeScreen::class.java)

            startActivity(intent)

            finish()
        }
        binding.signupBtn.setOnClickListener {


            val intent =
                Intent(this, Supplier_Signup_Screen::class.java)

            startActivity(intent)

            finish()
        }

    }
}