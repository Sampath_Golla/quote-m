package com.tekmob.quote_m.suppliers.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.tekmob.quote_m.HomeScreen
import com.tekmob.quote_m.databinding.*
import com.tekmob.quote_m.suppliers.Supplier_HomeScreen

/**
 *  Created by Sucharitha Peddinti on 28/11/21.
 */
class Supplier_Otp_Screen : Activity() {
    private lateinit var binding: SupplierOtpActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SupplierOtpActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.otpBtn.setOnClickListener {


            val intent =
                Intent(this, Supplier_HomeScreen::class.java)

            startActivity(intent)

            finish()
        }

    }
}