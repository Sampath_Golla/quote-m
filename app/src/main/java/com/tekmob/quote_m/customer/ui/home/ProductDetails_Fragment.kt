package com.tekmob.quote_m.customer.ui.home

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.tekmob.quote_m.R
import com.tekmob.quote_m.databinding.ProductDetailActivityBinding

class ProductDetails_Fragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: ProductDetailActivityBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = ProductDetailActivityBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.nextBtn.setOnClickListener {
            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_home_screen
                )
            navController.navigate(R.id.nav_r_quoute_details)

        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}