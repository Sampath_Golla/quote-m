package com.tekmob.quote_m.customer.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.tekmob.quote_m.HomeScreen
import com.tekmob.quote_m.databinding.CustomerLoginActivityBinding
import com.tekmob.quote_m.databinding.CustomerRegistrationActivityBinding
import com.tekmob.quote_m.databinding.DashboardActivityBinding
import com.tekmob.quote_m.databinding.OtpActivityBinding

/**
 *  Created by Sucharitha Peddinti on 28/11/21.
 */
class Otp_Screen : Activity() {
    private lateinit var binding: OtpActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OtpActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.otpBtn.setOnClickListener {


            val intent =
                Intent(this, HomeScreen::class.java)

            startActivity(intent)

            finish()
        }

    }
}