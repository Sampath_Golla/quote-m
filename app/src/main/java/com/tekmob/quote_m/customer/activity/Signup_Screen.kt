package com.tekmob.quote_m.customer.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.tekmob.quote_m.databinding.CustomerRegistrationActivityBinding
import com.tekmob.quote_m.suppliers.activity.Supplier_Signin_Screen

/**
 *  Created by Sucharitha Peddinti on 28/11/21.
 */
class Signup_Screen : Activity() {
    private lateinit var binding: CustomerRegistrationActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CustomerRegistrationActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.signinBtn.setOnClickListener {


            val intent =
                Intent(this, Signin_Screen::class.java)

            startActivity(intent)

            finish()
        }
        binding.signupBtn.setOnClickListener {


            val intent = Intent(this, Signin_Screen::class.java)

            startActivity(intent)

            finish()
        }

    }
}