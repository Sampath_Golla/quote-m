package com.tekmob.quote_m.customer.ui.orders

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.tekmob.quote_m.R
import com.tekmob.quote_m.databinding.MyordersActivityBinding
import com.tekmob.quote_m.ui.gallery.ProfileViewModel

class MyOrdersFragment : Fragment() {

    private lateinit var profileViewModel: ProfileViewModel
    private var _binding: MyordersActivityBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        profileViewModel =
            ViewModelProvider(this).get(ProfileViewModel::class.java)

        _binding = MyordersActivityBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.orderdetailsLin.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_home_screen
                )
            navController.navigate(R.id.nav_myorders_details)
        }

        binding.awardedLinear.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_home_screen
                )
            navController.navigate(R.id.nav_award)
        }
        binding.deliverLinear.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_home_screen
                )
            navController.navigate(R.id.nav_supplierdelivered)
        }

        binding.invoiceLinear.setOnClickListener {

            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_home_screen
                )
            navController.navigate(R.id.nav_invoice)
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}