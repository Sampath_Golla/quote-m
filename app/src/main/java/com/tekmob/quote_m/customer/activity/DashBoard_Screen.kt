package com.tekmob.quote_m.customer.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.tekmob.quote_m.databinding.DashboardActivityBinding
import com.tekmob.quote_m.suppliers.Supplier_HomeScreen
import com.tekmob.quote_m.suppliers.activity.Supplier_Signup_Screen

/**
 *  Created by Sucharitha Peddinti on 28/11/21.
 */
class DashBoard_Screen : Activity() {
    private lateinit var binding: DashboardActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DashboardActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.customerLogin.setOnClickListener {


            val intent =
                Intent(this, Signup_Screen::class.java)

            startActivity(intent)

            finish()
        }
        binding.supplierLogin.setOnClickListener {


            val intent =
                Intent(this, Supplier_Signup_Screen::class.java)

            startActivity(intent)

            finish()
        }

    }
}